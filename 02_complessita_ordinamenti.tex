\section{Studio della complessità di algoritmi d'ordinamento}
\paragraph*{}In questa sezione saranno analizzati tre classici algoritmi per l'ordinamento di elementi interi non negativi all'interno di un vettore: insertion sort, merge sort e selection sort.

\subsection{Insertion Sort}
\paragraph*{}

\begin{lstlisting}[caption={Insertion Sort},label={lst:insert_sort}]
FOR j = 2 TO length(A)
	key = A[j]
	i = j - 1
	WHILE i > 0 and a[i] > key
		A[i+1] = A[i]
		i = i - 1
	A[i+1] = key
\end{lstlisting}

\begin{table}[h!]
	\centering
	\begin{tabular}{ c | c | c }
		\textbf{Linea} & \textbf{Num. op. elementari} & \textbf{Contributi} \\ 
		1 & $c_1$ & $n$ \\
		2 & $c_2$ & $n-1$ \\
		3 & $c_3$ & $n-1$ \\ 
		4 & $c_4$ & $\sum_{j=2}^{n}t_j$ \\
		5 & $c_5$ & $\sum_{j=2}^{n}(t_j-1)$ \\
		6 & $c_6$ & $\sum_{j=2}^{n}(t_j-1)$ \\
		7 & $c_7$ & $n-1$ \\ 
	\end{tabular}
	\caption{Calcolo dei contributi per l'algoritmo mostrato nel listing \ref{lst:insert_sort}}
	\label{tbl:insert_sort}
\end{table}

\paragraph*{}La complessità calcolata per l'algoritmo \ref{lst:insert_sort} è:
$T(n) = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}t_j + c_5 \cdot \sum_{j=2}^{n}(t_j-1) + c_6 \cdot \sum_{j=2}^{n}(t_j-1) +  c_7 \cdot (n-1)$.
\begin{tcolorbox}[colback=green!5!white,colframe=green!75!black,title=Per fare chiarezza,fonttitle=\bfseries]
	Diversamente da quanto accade per il costrutto \texttt{\textcolor{blue}{FOR}}, generalmente il numero di iterazioni compiute dall'istruzione \texttt{\textcolor{blue}{WHILE}} non è predeterminato ed inoltre, in questo caso particolare, il secondo è incluso nel primo.\\Per questo motivo per rappresentare l'espressione che quantifichi il numero di esecuzioni, è necessario adoperare una notazione che rifletta la natura di ambo i cicli.\\
	Siano $t_2$, $t_3$, $\dots$, $t_n$ il numero di esecuzioni di una riga rispettivamente per $j=2$, $j=3$, $\dots$, $j=n$, allora si scrive $$\sum_{j=2}^{n} t_j$$ laddove i termini $n$ e $j=2$ della sommatoria riflettono i limiti del ciclo \texttt{\textcolor{blue}{FOR}} esterno e $t_j$ è il numero di esecuzioni determinato dal \texttt{\textcolor{blue}{WHILE}} per un fissato valore di $j$.
\end{tcolorbox}
\paragraph*{}L'espressione $T(n)$ rappresenterebbe il valore dell'analisi se per ogni $j$, conoscessimo il rispettivo valore $t_j$, ma questa funzione non dipende esclusivamente da $n$, come possiamo fare a trovare una forma chiusa?
\paragraph*{}Benché le istanze che possono presentarsi siano infinite e quindi troppe per poterle analizzare una ad una, possiamo farle ricadere in tre grandi famiglie e quindi essere sequenze da
\begin{description}
	\item [caso migliore] istanze che minimizzino il lavoro dell'algoritmo
	\item [caso peggiore] istanze che massimizzino il lavoro dell'algoritmo
	\item [caso medio] istanze rimanenti
\end{description}

\subsubsection{Caso migliore} 
\paragraph*{}Per l'algoritmo in questione, il caso migliore che si possa presentare è quello per cui ogni elemento della sequenza è già posto in ordine crescente. Di conseguenza l'istruzione \texttt{\textcolor{blue}{WHILE}} farà solo un'esecuzione per il controllo della sua condizione che risulta sempre falsa. Quindi risulta $\bm{t_j = 1},\,\forall j=2,\dots, n$, da cui:

\begin{dmath*}
T(n) = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}t_j + c_5 \cdot \sum_{j=2}^{n}(t_j-1) + c_6 \cdot \sum_{j=2}^{n}(t_j-1) +  c_7 \cdot (n-1)
	 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot (n-1) + c_5 \cdot \cancel{\sum_{j=2}^{n}(1-1)} + c_6 \cdot \cancel{\sum_{j=2}^{n}(1-1)} +  c_7 \cdot (n-1)
	 = c_1\cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot (n-1) + c_7 \cdot (n-1)
	 = n \cdot (c_1+c_2+c_3+c_4+c_7) - (c_2+c_3+c_4+c_7)
	 = a\cdot n + b = \Theta(n).
\end{dmath*}

\begin{tcolorbox}[colback=yellow!5!white,colframe=orange!75!black,title=Ricordando che...,fonttitle=\bfseries]
	Prima di affrontare i calcoli per la valutazione del caso peggiore, teniamo ben a mente le seguenti:
	\begin{itemize}
		\item $\sum_{\bm{i=2}}^{n}i=\sum_{\bm{i=1}}^{n}(i)-1=\dfrac{1}{2}n(n+1)-1$
		\item $\sum_{\bm{i=2}}^n(i-1) =\sum_{\bm{i=2}}^{n}i - \sum_{\bm{i=2}}^{n}1=\dfrac{1}{2}n(n-1)$
	\end{itemize}
\end{tcolorbox}

\subsubsection{Caso peggiore}
\paragraph*{}Per l'algoritmo il caso peggiore è rappresentato da quell'istanze che hanno tutti gli elementi posizionati in ordine decrescente. Ad ogni ciclo, il valore di $j$ è sempre massimizzato, quindi $\bm{t_j = j},\,\forall j=2,\dots, n$, pertanto:
\begin{dmath*}
	T(n) = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}t_j + c_5 \cdot \sum_{j=2}^{n}(t_j-1) + c_6 \cdot \sum_{j=2}^{n}(t_j-1) +  c_7 \cdot (n-1)
		 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}j + c_5 \cdot \sum_{j=2}^{n}(j-1) + c_6 \cdot \sum_{j=2}^{n}(j-1) +  c_7 \cdot (n-1)
		 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \left[\dfrac{1}{2}n^2+\dfrac{1}{2}n-1\right] + (c_5+c_6) \cdot \left[\dfrac{1}{2}n^2+\dfrac{1}{2}n\right]+c_7(n-1)
		 = \dfrac{1}{2}n^2(c_4+c_5+c_6)+\dfrac{1}{2}n\cdot(2c_1+2c_2+2c_3+c_4-c_5-c_6)-(c_2+c_3+c_4+c_7)
		 = a^2n+bn+c = \Theta(n^2)
\end{dmath*}

\begin{tcolorbox}[colback=yellow!5!white,colframe=orange!75!black,title=Ricordando che...,fonttitle=\bfseries]
	Sia $k$ una costante moltiplicativa, una delle proprietà della sommatoria consente questa equivalenza:
	$$\sum_{\bm{i=0}}^{n}k\cdot i=k\cdot \left[\sum_{\bm{i=0}}^{n}i\right]$$
\end{tcolorbox}

\subsubsection{Caso medio} L'analisi del caso migliore e del caso peggiore determinano dei risultati tra loro decisamente discordanti e quindi non conclusivi. È necessario studiare il caso medio. Nel caso di questo algoritmo, il caso medio è rappresentato dall'insieme di istanze per le quali solo la metà degli elementi risulta essere già ordinato e dunque in media controlliamo solamente metà del sotto-array. Pertanto $\bm{t_j = \frac{j}{2}}$ e quindi:

\begin{dmath*}
	T(n) = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}t_j + c_5 \cdot \sum_{j=2}^{n}(t_j-1) + c_6 \cdot \sum_{j=2}^{n}(t_j-1) +  c_7 \cdot (n-1)
		 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \sum_{j=2}^{n}\dfrac{j}{2} + c_5 \cdot \sum_{j=2}^{n}{\left(\dfrac{j}{2}-1\right)}+c_6 \cdot \sum_{j=2}^{n}{\left(\dfrac{j}{2}-1\right)}+ c_7 \cdot (n-1)
		 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + \dfrac{c_4}{2} \sum_{j=2}^{n}{j} + (c_5+c_6) \cdot \left(\frac{1}{2}\sum_{j=2}^{n}{j} - \sum_{j=2}^{n}{1}\right)+  c_7 \cdot (n-1)
		 = c_1 \cdot n + c_2 \cdot (n-1) + c_3 \cdot (n-1) + c_4 \cdot \dfrac{n(n+1-2)}{4} + (c_5+c_6) \cdot \dfrac{n^2-3n+2}{4} +  c_7 \cdot (n-1)
		 = {n^2\cdot \left(\dfrac{c_4+c_5+c_6}{4}\right) + n\cdot \left[\dfrac{4(c_1+c_2+c_3-c_7) + c_4- 3(c_5+c_6)}{4}\right]-\left(\dfrac{2(c_2+c_3+c_7)-(c_4+c_5+c_6)}{2}\right)}
		 = an^2+bn+c = \Theta(n^2)
\end{dmath*}