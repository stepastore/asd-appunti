\section{Definizioni preliminari}

\subsection{Nozioni fondamentali sugli algoritmi}
\paragraph*{\textcolor{t1}{Algoritmo}} Un \textbf{algoritmo} è una procedura ben definita per risolvere un problema: una sequenza \textbf{descritta in modo finito} di passi che, se eseguiti da un esecutore, portano alla soluzione.

\paragraph*{\textcolor{t2}{Alcune proprietà}}
\begin{multicols}{2}
	\begin{itemize}
		\item \textcolor{t3}{\textbf{non ambiguità}}: tutti i passi devono essere non ambigui e comprensibili;
		\item \textcolor{t3}{\textbf{correttezza}}: produce il risultato corretto a fronte di qualsiasi istanza;
		\item \textcolor{t3}{\textbf{generalità}}: algoritmo risolve qualsiasi istanza legittima del problema;
		\item \textcolor{t3}{\textbf{efficienza}}: misura le risorse che l'algoritmo impiega — tempo, memoria, banda....
	\end{itemize}
\end{multicols}
%
\paragraph*{\textcolor{t1}{Tempo di esecuzione}} Il \textbf{tempo di esecuzione} di un algoritmo dipende da:
\begin{multicols}{2}
	\begin{itemize}
		\item macchina hardware;
		\item compilatore/interprete;
		\item tipo e dimensione dell'input;
		\item altri fattori — casualità, livello di utilizzo della macchina.
	\end{itemize}
\end{multicols}
Al fine di analizzare il \textbf{tempo di esecuzione intrinseco} impiegato da un algoritmo si utilizza un modello computazionale (Macchina di Turing, Modello RAM) che risulta essere nei fatti una semplificazione dei moderni computer con le seguenti caratteristiche:
\begin{itemize}
	\item memoria principale infinita e ad accesso diretto;
	\item singolo processore con programma;
	\item lettura, scrittura, operazioni elementari avvengono in una unità di tempo.
\end{itemize}
%
\paragraph*{\textcolor{t1}{Linee guida per il calcolo del tempo di esecuzione}} Le seguenti regole devono essere osservate per il corretto calcolo del tempo di esecuzione di un algoritmo:
\begin{itemize}
	\item per ogni linea si calcola il numero di operazioni elementari che essa comporta;
	\item se una linea viene eseguita più volte — ad esempio perché in un ciclo — è necessario calcolare quante volte viene ripetuta;
	\item si sommano i contributi delle linee.
\end{itemize}
%
\pagebreak
\paragraph*{\textcolor{t1}{Un primo esempio}} Nell'esempio che segue, analizziamo il tempo di esecuzione di un algoritmo. Per ogni linea vengono calcolati i relativi contributi, riportati nella tabella sottostante.

\begin{lstlisting}[caption={Algoritmo con esempio di calcolo dei contributi},label={lst:first_alg}]
int Count_0(int N)
{
	sum = 0
	FOR i = 1 TO N
		FOR j = 1 TO N
			IF i <= j THEN
				sum = sum + 1
	RETURN sum
}
\end{lstlisting}

\begin{table}[h]
	\begin{tabular}{ c | c }
		\centering
		\textbf{Linea} & \textbf{Contributi} \\ 
		3 & $1$ \\ 
		4 & $2\sum^{N+1}_{i=1}1 = 2(N+1)$ \\
		5 & $2\sum^{N}_{i=1} \sum_{j=1}^{N+1}=2\sum_{i=1}^{N}(N+1)$ \\
		6 & $2\sum_{i=1}^{N}\sum_{j=1}^{N}1=2\sum_{i=1}^{N}N$ \\
		7 & $\sum_{n=1}^{N} (N-i+1) =\sum_{i=1}^{N}(N+1) - \sum_{i=1}^{N} i = N\cdot (N+1) - \frac{N(N+1)}{2}=\frac{N(N+1)}{2}$\\ 
		8 & $1$\\
	\end{tabular}
	\caption{Calcolo dei contributi per l'algoritmo mostrato nel listing \ref{lst:first_alg}}
	\label{tbl:first_alg}
\end{table}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black,title=Attenzione,fonttitle=\bfseries]
Fissato $i$, la riga 7 viene eseguita per tutti i valori di $j$ per i quali \linebreak $i \leq j \leq N$, cioè $\sum_{k=i}^{N}1 = (N-i+1)$.
\end{tcolorbox}

\subparagraph{\textcolor{t2}{Calcolo della somma}} Per ottenere la stima del tempo di esecuzione, dobbiamo sommare ogni contributo:

\begin{dmath*}
T(n) = 1+2\cdot (N+1) + 2N\cdot (N+1) + 2N^2+\dfrac{N\cdot(N+1)}{2}+1
	 = 2 + 2N + 2 + 2N^2 + 2N+2N^2+\dfrac{N^2}{2}+\dfrac{N}{2}
	 =\dfrac{9}{2}N^2+\dfrac{9}{2}N+4
\end{dmath*}

\subsection{Limiti asintotici} Sia dato un algoritmo. Per input sufficientemente grandi, le costanti moltiplicative e i termini di ordine inferiore, che compaiono in un tempo esatto di esecuzione, sono dominati dagli effetti della dimensione stessa dell'input. Quando si opera con dimensioni di input grandi abbastanza da rendere rilevante soltanto il tasso di crescita del tempo di esecuzione, stiamo studiando l'efficienza asintotica degli algoritmi. Cioè interessa sapere come aumenta il tempo di esecuzione di un algoritmo al crescere della dimensione dell'input, quando essa tende all'infinito.
\subsubsection{Definizioni di $\Theta$, $O$ e $\Omega$}
Per le prossime definizioni, si considerino definite due funzioni $f(n)$ e $g(n)$.
\paragraph*{\textcolor{t1}{Limite superiore asintotico}} Intuitivamente con la notazione $O(.)$ si denota quella classe di funzioni che giacciono sempre al di sopra della funzione, che abbiamo in esame, a partire da un fissato $n_0$ in poi.
\subparagraph*{} La funzione $g(n)$ è detta essere \textbf{limite superiore asintotico} di $f(n)$ e si caratterizza con $O(g(n)) = f(n)$ il seguente insieme:
$$O(g(n)) = \left\{f(n) \mid \exists c > 0 \text{ ed } n_0 > 0 \text{ costanti } : 0 \leq f(n) \leq c\cdot g(n),\;\forall n \geq n_0 \right\}$$
%
\paragraph*{\textcolor{t1}{Limite inferiore asintotico}} Istintivamente anticipiamo che con la scrittura $\Omega(.)$ si denota quella classe di funzioni che permangono sempre al di sotto della funzione oggetto di studio, a partire da un fissato $n_0$ in poi.
\subparagraph*{} La funzione $g(n)$ è detta essere \textbf{limite inferiore asintotico} di $f(n)$ e si caratterizza con $\Omega(g(n)) = f(n)$ il seguente insieme:
$$\Omega(g(n)) = \left\{f(n) \mid \exists c > 0 \text{ ed } n_0 > 0 \text{ costanti } : 0 \leq c\cdot g(n)\leq f(n),\;\forall n \geq n_0 \right\}$$
%
\paragraph*{\textcolor{t1}{Limite asintotico stretto}} Questo limite asintotico è una commistione dei due precedenti.
\subparagraph*{} La funzione $g(n)$ rappresenta un \textbf{limite asintotico stretto} per $f(n)$ e si caratterizza con $\Theta(g(n)) = f(n)$ il seguente insieme:
$$\Theta(g(n)) = \left\{f(n) \mid \exists c_1 > 0, c_2 > 0 \text{ ed } n_0 > 0 \text{ costanti } : 0 \leq c_1\cdot g(n)\leq f(n) \leq c_2 \cdot g(n),\;\forall n \geq n_0 \right\}$$
\subsubsection{Teoremi}
Alcuni teoremi che comprendono uguaglianze ed implicazioni utili:
\begin{itemize}
	\item $f(n) = O(g(n)) \Longleftrightarrow g(n) = \Omega(f(n))$
	\item $f_1(n) = O(f_2(n)) \wedge f_2(n) = O(f_3(n)) \Longrightarrow f_1(n) = O(f_3(n))$
	\item $f_1(n) = \Omega(f_2(n)) \wedge f_2(n) = \Omega(f_3(n)) \Longrightarrow f_1(n) = \Omega(f_3(n))$
	\item $f_1(n) = \Theta(f_2(n)) \wedge f_2(n) = \Theta(f_3(n)) \Longrightarrow f_1(n) = \Theta(f_3(n))$
	\item $f_1 = O(g_1(n)) \wedge f_2=O(g_2(n)) \Longrightarrow O(f_1+f_2) = O(\max\{g_1(n), g_2(n)\})$
	\item $f(n)$ è un polinomio di grado $\delta \Longleftarrow f(n) = \Theta(n^{\delta})$
	\item $f(n) = \Theta(g(n)) \Longleftrightarrow f(n) = O(g(n)) \wedge f(n) = \Omega(g(n))$
\end{itemize}
\subsubsection{Alcune proprietà}
Di seguito si riportano alcune proprietà utili dei limiti di funzione in relazione alle definizione di limiti asintotici:
\begin{itemize}
	\item $\lim_{n \to \infty} \dfrac{f(n)}{g(n)} = 0 \Longrightarrow f(n) = O(g(n))$
	\item $\lim_{n \to \infty} \dfrac{f(n)}{g(n)} = \infty \Longrightarrow f(n) = \Omega(g(n))$
	\item $\lim_{n \to \infty} \dfrac{f(n)}{g(n)} = k,\,k>0 \Longrightarrow f(n) = \Theta(g(n))$
\end{itemize}
%
\subsection{Massima somma di elementi contigui in un array}
\begin{lstlisting}[caption={Calcolo della massima somma tra numeri contigui in array},label={lst:somma_cont_array}]
int Max_Seq_Sum(int N, A[])
{
	maxsum = 0
	sum = 0
	FOR j = 1 TO N
		IF (sum + A[j] > 0) THEN
			sum = sum + A[j]
		ELSE
			sum = 0
		maxsum = Max(maxsum, max)
	RETURN maxsum
}
\end{lstlisting}